ARG BASE_IMAGE=atlas/analysisbase:22.2.72
FROM ${BASE_IMAGE} as base

WORKDIR /

ENV PATH=/home/atlas/.local/bin:"${PATH}"
COPY requirements.txt /
COPY requirements.lock /

# Use Brett Cannon's recommendations for pip-secure-install to ensure environment
# is reproducible and installed as securely as possible.
# c.f. https://www.python.org/dev/peps/pep-0665/#secure-by-design
# c.f. https://github.com/brettcannon/pip-secure-install
# c.f. https://twitter.com/brettsky/status/1486137764315688961
RUN . /release_setup.sh && \
    python3 -m pip --no-cache-dir install --upgrade pip setuptools wheel && \
    python3 -m pip --no-cache-dir install \
        --no-deps \
        --require-hashes \
        --only-binary :all: \
        --no-binary pyspark \
        --requirement requirements.lock

WORKDIR /workdir
