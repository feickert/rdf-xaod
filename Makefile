default: image

all: image

image:
	docker pull atlas/analysisbase:22.2.72
	docker build . \
	--file Dockerfile \
	--tag atlasamglab/rdf-xaod:latest
