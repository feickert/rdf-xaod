# rdf-xaod

Docker Images for RDF-xAOD studies with Dask and Spark


## Docker Images

- [`atlasamglab/rdf-xaod`][rdf-xaod-docker-hub]: ATLAS AnalysisBase with Dask and PySpark

## Build

To compile the lock file and build the Docker image locally:

``` console
$ bash compile_dependencies.sh
$ make
```

[rdf-xaod-docker-hub]: https://hub.docker.com/r/atlasamglab/rdf-xaod
